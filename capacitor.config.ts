import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'nba.stat.mg',
  appName: 'Stat NBA',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;

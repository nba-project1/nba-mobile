import React from 'react';
import { useParams } from 'react-router-dom';
import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent } from '@ionic/react';
import teamsData from '../data/data.json';

const TeamDetails: React.FC = () => {
  const { teamId } = useParams<{ teamId: string }>();
  const team = teamsData.teams.find(t => t.id === parseInt(teamId, 10));

  if (!team) {
    return <div>Team not found</div>;
  }

  return (
    <IonContent>
      <IonCard>
        <IonCardHeader>
          <IonCardTitle>{team.name} Details</IonCardTitle>
        </IonCardHeader>

        {team.players.map(player => (
          <IonCard key={player.id}>
            <IonCardHeader>
              <IonCardTitle>{player.name}</IonCardTitle>
              <IonCardSubtitle>Position: {player.position}</IonCardSubtitle>
            </IonCardHeader>

            <IonCardContent>
              <p>Stats:</p>
              <ul>
                <li>Points: {player.stats.points}</li>
                <li>Rebounds: {player.stats.rebounds}</li>
                <li>Assists: {player.stats.assists}</li>
                <li>Field Goal Percentage: {player.stats.fieldGoalPercentage}%</li>
              </ul>
            </IonCardContent>
          </IonCard>
        ))}
      </IonCard>
    </IonContent>
  );
};

export default TeamDetails;
import React from 'react';
import { IonList, IonItem, IonLabel } from '@ionic/react';
import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonRouterLink } from '@ionic/react';
import teams from '../data/data.json';

const MyList = () => {
  const items = teams.teams;

  return (
    // <IonList>
    //   {items.map(item => (
    //     <IonItem key={item.id}>
    //       <IonLabel>{item.text}</IonLabel>
    //     </IonItem>
    //   ))}
    // </IonList>
    <>
    <IonList>
    {items.map(item => (
      <IonRouterLink key={item.id} href={`/teams/${item.id}`}>
        <IonCard color="light" >
          <IonCardHeader>
            <IonCardTitle>{item.name}</IonCardTitle>
      {/* <IonCardSubtitle>Card Subtitle</IonCardSubtitle> */}
          </IonCardHeader>

    {/* <IonCardContent>Card Content</IonCardContent> */}
        </IonCard>
        </IonRouterLink>
  ))}</IonList></>
  );
};

export default MyList;